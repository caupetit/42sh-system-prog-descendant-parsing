# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/24 06:29:29 by rbernand          #+#    #+#              #
#    Updated: 2014/03/27 18:58:40 by rbernand         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=42sh
CC=	cc
FLAGS=-Wall -Wextra -Werror -O3 -ggdb
LIB=libft/
INCLUDES=includes/
DIROBJ=objs/
DIRSRC=srcs/
SRC=analyser.c \
	cd.c \
	echo.c \
	env.c \
	env_fct.c \
	error.c \
	exec_and.c \
	exec_cmd.c \
	exec_left.c \
	exec_or.c \
	exec_pipe.c \
	exec_rright.c \
	exec_tree.c \
	exec_word.c \
	exit.c \
	fork_fct.c \
	jobs.c \
	lex_automaton.c \
	lex_func.c \
	lex_func2.c \
	lex_lst.c \
	main.c \
	outpout.c \
	outpout_color.c \
	pars_func.c \
	pars_func2.c \
	pars_tree.c \
	prompt.c \
	setenv.c \
	signal.c \
	singleton.c \
	term_cursor.c \
	term_eof.c \
	term_get_line.c \
	term_key_delete.c \
	term_move_cursor.c \
	term_output.c \
	term_termios.c \
	term_winsize.c \
	tools.c \
	unsetenv.c \
	wait.c
OBJ=$(SRC:%.c=$(DIROBJ)%.o)

all: init $(NAME) end

init:
	@tput init
	@make -s -C $(LIB)

end:
	@echo "\033[2K\t\033[1;36m$(NAME)\t\t\033[0;32m[Ready]\033[0m"

$(NAME): $(OBJ)
	@echo "\033[2KCompiling $(NAME) : "
	@$(CC) $(FLAGS) -o $@ $^ -I$(INCLUDES) -L$(LIB) -lft -ltermcap
	@tput cuu1

$(DIROBJ)%.o: $(DIRSRC)%.c
	@echo "\r\033[2KCompiling $< : "
	@$(CC) $(FLAGS) -o $@ -c $< -I$(INCLUDES)
	@tput cuu1

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -f $(NAME)

re: fclean all

