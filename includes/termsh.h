/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termsh.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 13:38:03 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/26 23:29:47 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TERMSH_H
# define TERMSH_H
# define K0					key[0]
# define K1					key[1]
# define K2					key[2]
# define K3					key[3]

typedef struct				s_char
{
	char					c;
	struct s_char			*next;
	struct s_char			*prev;
}							t_char;

typedef struct				s_cursor
{
	int						pos_col;
	t_char					*start;
	t_char					*current;
	t_char					*end;
}							t_cursor;

extern struct winsize		g_winsize;
extern struct termios		*g_backup_term;

char						*get_line(int len);

void						move_cursor(t_cursor *cursor, char key[4]);
void						term_delete(t_cursor *cursor);
void						term_backspace(t_cursor *cursor);
void						term_eof(void);

void						print_lst(t_cursor *cursor);
t_cursor					*init_cursor(int len);
void						init_winsize(struct winsize *winsize);
void						insert_char(t_cursor *cursor, char c);
int							init_term(void);
void						exit_term(void);

#endif
