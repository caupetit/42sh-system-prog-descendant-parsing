/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   analyser.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/03 15:12:09 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/27 18:54:26 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ANALYSER_H
# define ANALYSER_H

/*
**						GRAMMAR USED FOR 42SH
**	S			==	[Exp]+
**	Exp			==	andexp
**	andexp		==	sepexp [[&&] andexp]?
**	sepexp		==	orexp [[;] sepexp]?
**	orexp		==	right_rexp [[||] orexp]?
**	right_rexp	==	pipeexp [[> | >>] file]?
**	pipeexp		==	left_rexp [[|] pipeexp]?
**	left_rexp	==	[[<] file] ]? command [[<] file] ]?
**	command		==	word
**	file		==	word
*/

# define PARS_AND_NOCMD "Near &&, command needed."
# define PARS_OR_NOCMD "Near ||, command needed."
# define PARS_RIGHT_NOCMD "Near > or >>, command needed."
# define PARS_RIGHT_NOFILE "Near > or >>, file needed."
# define PARS_PIPE_NOCMD "Near |, command needed."
# define PARS_LEFT_USAGE "<: usage: \"< file command\" or \"command < file\"."

/*
**	enum for lexer automaton
**	  0     1     2     3      4      5      6      7     8
*/
enum				e_status
{
	_wait, _and, _sep, _or, _right, _pipe, _left, _word, _end
};

/*
**	enum for lex errors
*/
enum				e_lex_error
{
	_err_lex
};

/*
**	chained list for lexing
*/
typedef struct		s_lex
{
	int				type;
	char			*str;
	struct s_lex	*next;
	struct s_lex	*prev;
}					t_lex;

/*
**	data struct for lexer automaton
*/
typedef struct		s_auto
{
	int				status;
	int				i;
	char			*buf;
	t_lex			*l_lex;
}					t_auto;

/*
**	function pointer for lexer automaton
*/
typedef int		(*t_func)(t_auto *, char);

/*
**	tree list for parsing and execution
*/
typedef struct		s_tree
{
	int				type;
	t_lex			*list;
	struct s_tree	*left;
	struct s_tree	*right;
}					t_tree;

/*
**	lex_lst.c
*/
void				lex_lst_del(t_lex **l_lex);
t_lex				*lex_lst_new(t_auto *aut);
int					lex_lst_len(t_lex *lst);
void				lex_lst_add(t_lex **l_lex, t_lex *new);

/*
**	lex_automaton.c
*/
int					lex_error(t_auto *aut);
int					lex_init_auto(t_auto *aut, int size);
int					lex_add_token(t_auto *aut);
int					lex_automaton(t_auto *aut, char c);

/*
**	lex_func.c
*/
int					lex_wait(t_auto *aut, char c);
int					lex_and(t_auto *aut, char c);
int					lex_sep(t_auto *aut, char c);
int					lex_or(t_auto *aut, char c);
int					lex_right(t_auto *aut, char c);

/*
**	lex_func2.c
*/
int					lex_pipe(t_auto *aut, char c);
int					lex_left(t_auto *aut, char c);
int					lex_word(t_auto *aut, char c);

/*
**	pars_tree.c
*/
t_tree				*pars_tree_new(t_lex *l_lex);
int					pars_tree_add(t_tree *tree, t_lex *tmp, int type);
void				pars_tree_del(t_tree **tree);

/*
**	analyser.c
*/
int					pars_error(char *str);
t_tree				*pars(t_lex *l_lex);
t_tree				*analyzer(char *line);

/*
**	pars_func.c
*/
int					pars_and(t_tree *tree);
int					pars_sep(t_tree *tree);
int					pars_or(t_tree *tree);
int					pars_right(t_tree *tree);
int					pars_pipe(t_tree *tree);

int					pars_left(t_tree *tree);

#endif
