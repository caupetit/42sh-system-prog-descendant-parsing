    /* ************************************************************************** */
    /*                                                                            */
    /*                                                        :::      ::::::::   */
    /*                                                      :+:      :+:    :+:   */
    /*                    42sh                            +:+ +:+         +:+     */
    /*                                                  +#+  +:+       +#+        */
    /*                                                +#+#+#+#+#+   +#+           */
    /*    By: caupetit <caupetit@student.42.fr>            #+#    #+#             */
    /*    By: rbernand <rbernand@student.42.fr>           ###   ########.fr       */
    /*                                                                            */
    /* ************************************************************************** */


# Welcome to 42sh project. #

****

This is a group project. Only rbernand and me worked on.

Works only on mac OSX. A simple implementation c standar functions is used. See [libft/](https://bitbucket.org/caupetit/philo-pthread-mutex/src/64c77e5a8120f4bb7035f644ba09bccd69939d98/libft/?at=master)

This program propose a simple implementation of a UNIX SHELL.

 - LEXING with AUTOMATONS.

 - LL grammar: see [analyser.h](https://bitbucket.org/caupetit/42sh-system-prog-descendant-parsing/src/569d8fb7cb1c4acd8f26a2ba86512a09c048c3f1/includes/analyser.h?at=master)

 - Recursive descent PARSER.

 - TERMCAPS display.

****

Use:

 - make

 - ./42sh

****

Here is an exemple of our 42sh in use:

![Screen Shot - 42sh.png](https://bitbucket.org/repo/oAqGnA/images/429586257-Screen%20Shot%20-%2042sh.png)