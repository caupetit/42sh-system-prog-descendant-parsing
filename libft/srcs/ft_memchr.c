/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 15:11:47 by rbernand          #+#    #+#             */
/*   Updated: 2013/12/03 16:58:15 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <libft.h>

void		*ft_memchr(const void *s, int c, size_t n)
{
	size_t		index;

	index = 0;
	while (index < n)
	{
		if ((unsigned char)c == *((unsigned char*)s + index))
			return ((unsigned char*)s + index);
		index++;
	}
	return (NULL);
}
