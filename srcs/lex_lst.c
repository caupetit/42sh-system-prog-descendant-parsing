/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_lst.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 18:57:24 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/27 18:54:41 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <shell.h>
#include <analyser.h>

int			lex_lst_len(t_lex *lst)
{
	t_lex	*tmp;
	int		i;

	tmp = lst;
	i = 0;
	while (tmp)
	{
		tmp = tmp->next;
		i++;
	}
	return (i);
}

void		lex_lst_del(t_lex **l_lex)
{
	t_lex	*tmp;
	t_lex	*tmp2;

	tmp = *l_lex;
	while (tmp)
	{
		ft_strdel(&tmp->str);
		tmp2 = tmp->next;
		free(tmp);
		tmp = tmp2;
	}
}

t_lex		*lex_lst_new(t_auto *aut)
{
	t_lex	*new;

	if (!(new = (t_lex *)sh_malloc(sizeof(t_lex))))
		return (NULL);
	new->type = aut->status;
	if (!(new->str = ft_strdup(aut->buf)))
	{
		free(new);
		return (NULL);
	}
	new->next = NULL;
	new->prev = NULL;
	return (new);
}

void		lex_lst_add(t_lex **l_lex, t_lex *new)
{
	t_lex	*tmp;

	tmp = *l_lex;
	if (!tmp)
	{
		*l_lex = new;
		return ;
	}
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = new;
	new->next = NULL;
	new->prev = tmp;
}
