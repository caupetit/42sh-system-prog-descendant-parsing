/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_automaton.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/05 16:49:17 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/27 16:45:53 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <shell.h>
#include <analyser.h>

int			lex_error(t_auto *aut)
{
	ft_putstr_fd("Unauthorized operator: ", 2);
	ft_putendl_fd(aut->buf, 2);
	return (0);
}

int			lex_init_auto(t_auto *aut, int size)
{
	aut->status = _wait;
	aut->i = 0;
	if (!(aut->buf = (char *)sh_malloc((size + 1) * sizeof(char))))
		return (0);
	ft_bzero(aut->buf, size);
	aut->l_lex = NULL;
	return (1);
}

int			lex_add_token(t_auto *aut)
{
	t_lex	*new;

	if (!(new = lex_lst_new(aut)))
	{
		aut->l_lex = NULL;
		return (0);
	}
	lex_lst_add(&aut->l_lex, new);
	aut->i = 0;
	ft_bzero(aut->buf, ft_strlen(aut->buf));
	return (1);
}

int			lex_automaton(t_auto *aut, char c)
{
	static t_func	func[] =

	{
	lex_wait,
	lex_and,
	lex_sep,
	lex_or,
	lex_right,
	lex_pipe,
	lex_left,
	lex_word
	};
	if (!func[aut->status](aut, c))
	{
		lex_lst_del(&aut->l_lex);
		return (0);
	}
	return (1);
}
