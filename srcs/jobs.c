/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   jobs.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/25 20:32:13 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/25 20:34:23 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <shell.h>

int		jobs42(char **args)
{
	t_fork		*tmp;

	(void)args;
	tmp = g_fork;
	while (tmp)
	{
		ft_printf("%d\n", tmp->pid);
		tmp = tmp->next;
	}
	return (0);
}
