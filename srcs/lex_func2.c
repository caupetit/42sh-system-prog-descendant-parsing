/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_func2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/05 16:57:19 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/26 23:01:32 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "analyser.h"

int			lex_pipe(t_auto *aut, char c)
{
	if (aut->i == 1 && c == '|')
	{
		aut->buf[aut->i] = c;
		aut->i += 1;
		aut->status = _or;
		return (1);
	}
	if (!lex_add_token(aut))
		return (0);
	aut->status = _wait;
	return (lex_wait(aut, c));
}

int			lex_left(t_auto *aut, char c)
{
	if (!lex_add_token(aut))
		return (0);
	aut->status = _wait;
	return (lex_wait(aut, c));
}

int			lex_word(t_auto *aut, char c)
{
	if (c == '&' || c == ';' || c == '|' || c == '>' || c == '<' || !c
		|| ft_iswhite(c))
	{
		if (!lex_add_token(aut))
			return (0);
		aut->status = _wait;
		return (lex_wait(aut, c));
	}
	aut->buf[aut->i] = c;
	aut->i += 1;
	aut->buf[aut->i] = 0;
	return (1);
}
