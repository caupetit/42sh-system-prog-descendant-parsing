/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/01 15:15:30 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 19:54:04 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <libft.h>
#include <termsh.h>
#include <shell.h>

void		modif_env(char **args, int *i, char *path)
{
	char	*tmp;

	g_env = *i == 1 ? g_env : NULL;
	while (args[*i] && ft_strchr(args[*i], '='))
	{
		tmp = ft_strcdup(args[*i], '=');
		setenv_exec(tmp, ft_strchr(args[*i], '=') + 1, 1);
		ft_strdel(&tmp);
		(*i)++;
	}
	if (args[*i])
	{
		args[*i] = get_bin(args[*i], path);
		exec_cmd(args + *i, path);
	}
	else
		put_env(g_env);
}

int			env(char **args)
{
	int		pid;
	int		i;
	char	*path;
	t_env	*env;

	if (args && args[0] && !args[1])
		put_env(g_env);
	else if (args && args[0] && args[1] && ft_strequ(args[1], "-i") && !args[2])
		put_env(NULL);
	else if (args && args[0] && args[1])
	{
		if ((env = get_var_env("PATH")))
			path = env->value;
		else
			path = NULL;
		if ((pid = my_fork()) == 0)
		{
			i = ft_strequ(args[1], "-i") ? 2 : 1;
			modif_env(args, &i, path);
			exit(0);
		}
		else
			my_wait(pid);
	}
	return (0);
}
