/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_basic.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 15:34:53 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 16:01:00 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <libft.h>
#include <shell.h>
#include <sys/wait.h>

int				exec_builtins(char **args, int i)
{
	static t_builtin_fct		tab_builtin_fct[] =

	{
	&cd,
	&env,
	&exit42,
	&setenv42,
	&unsetenv42,
	&echo42,
	&jobs42,
	NULL,
	};
	return (tab_builtin_fct[i](args));
}

int				is_builtin(char *name)
{
	int				i;
	static char		*builtins[] =

	{
	"cd",
	"env",
	"exit",
	"setenv",
	"unsetenv",
	"echo",
	NULL,
	};
	i = 0;
	while (builtins[i] && !ft_strequ(builtins[i], name))
		i++;
	if (!builtins[i])
		return (-1);
	return (i);
}

void			exec_word(t_tree *tree)
{
	int			pid;
	int			i;
	char		**args;
	t_env		*path;

	args = tree_to_tab(tree->list);
	if (args && (i = is_builtin(args[0])) != -1)
		g_last_return = exec_builtins(args, i);
	else
	{
		if ((pid = my_fork()) == 0)
		{
			path = get_var_env("PATH");
			if (path)
				exec_cmd(args, get_var_env("PATH")->value);
			else
				exec_cmd(args, NULL);
		}
		else
			my_wait(pid);
	}
	ft_tabdel(&args);
}
