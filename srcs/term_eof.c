/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_eof.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 20:41:19 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/26 23:14:05 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/ioctl.h>
#include <shell.h>

void		term_eof(void)
{
	static char	key[4] =

	{
	4,
	0,
	0,
	0,
	};
	if (g_fork)
		ioctl(0, TIOCSTI, key);
	else
		exit42(0);
}
