/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_key_delete.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 19:44:57 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/26 23:42:08 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <term.h>
#include <stdlib.h>
#include <termsh.h>

void			term_backspace(t_cursor *cursor)
{
	t_char		*todel;

	if (cursor->current->prev == NULL)
		return ;
	tputs(tgetstr("dm", NULL), 1, ft_putchar);
	tputs(tgetstr("le", NULL), 1, ft_putchar);
	tputs(tgetstr("cd", NULL), 1, ft_putchar);
	tputs(tgetstr("dc", NULL), 1, ft_putchar);
	tputs(tgetstr("ed", NULL), 1, ft_putchar);
	todel = cursor->current->prev;
	cursor->current->prev = todel->prev;
	if (todel->prev)
		todel->prev->next = cursor->current;
	else
		cursor->start = cursor->current;
	free(todel);
	print_lst(cursor);
}

void			term_delete(t_cursor *cursor)
{
	t_char		*todel;

	if (cursor->current == cursor->end)
		return ;
	tputs(tgetstr("dm", NULL), 1, ft_putchar);
	tputs(tgetstr("dc", NULL), 1, ft_putchar);
	tputs(tgetstr("cd", NULL), 1, ft_putchar);
	tputs(tgetstr("ed", NULL), 1, ft_putchar);
	todel = cursor->current;
	todel->next->prev = todel->prev;
	if (todel != cursor->start)
		todel->prev->next = todel->next;
	else
		cursor->start = todel->next;
	if (todel != cursor->start)
		cursor->current = todel->next;
	cursor->pos_col -= 1;
	free(todel);
	print_lst(cursor);
}
