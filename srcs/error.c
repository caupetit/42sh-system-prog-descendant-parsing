/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 19:20:34 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 16:32:21 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <shell.h>

static void		msg_error_exit(int num)
{
	if (num == 101)
		putendl_col(": Exit. Incopatible Terminal.", RED, NORMAL, 2);
	if (num == 102)
		putendl_col("Pipe impossible.", RED, NORMAL, 2);
	if (num == 103)
		putendl_col("Malloc error", RED, NORMAL, 2);
	exit(num);
}

int				msg_error(int num, char *str)
{
	putstr_col(str, RED, BOLD, 2);
	if (num == 1)
		putendl_col("Warning : environement not found.", RED, NORMAL, 2);
	else if (num == 2)
		putendl_col(": not found.", WHITE, NORMAL, 2);
	else if (num == 3)
		putendl_col("usage: setenv NAME VALUE [overwrite]", WHITE, NORMAL, 2);
	else if (num == 4)
		putendl_col("usage: unsetenv NAME1 [NAME2, ...]", WHITE, NORMAL, 2);
	else if (num == 7)
		putendl_col("Fork error.", RED, NORMAL, 2);
	else if (num == 8)
		putendl_col(": Command not found.", RED, NORMAL, 2);
	else if (num == 9)
		putendl_col(": Files not found or access denied.", RED, NORMAL, 2);
	else if (num == 10)
		putendl_col(": No termcap available.", RED, NORMAL, 2);
	else if (num > 100)
		msg_error_exit(num);
	return (num);
}
