/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sh_termios.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/06 15:42:09 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 16:54:54 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <term.h>
#include <termios.h>
#include <termsh.h>
#include <shell.h>

#include <libft.h>
#include <printf.h>

int				init_term(void)
{
	char			buffer[2048];
	struct termios	term;
	t_env			*term_env;

	term_env = get_var_env("TERM");
	if (term_env && term_env->value && tgetent(buffer, term_env->value) != 1)
	{
		msg_error(10, term_env->value);
		return (1);
	}
	if (!term_env)
	{
		msg_error(10, NULL);
		return (1);
	}
	tcgetattr(0, &term);
	tcgetattr(0, g_backup_term);
	term.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(0, 0, &term);
	return (0);
}

void			exit_term(void)
{
	struct termios	term;

	tcgetattr(0, &term);
	term.c_lflag |= ECHO;
	term.c_lflag |= ICANON;
	tcsetattr(0, 0, &term);
}
