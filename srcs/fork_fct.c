/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork_fct.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/06 10:16:08 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 16:45:27 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <libft.h>
#include <shell.h>

int		my_fork(void)
{
	t_fork		*new;
	int			child;

	child = fork();
	if (child == -1)
		msg_error(7, NULL);
	else if (child != 0)
	{
		new = (t_fork *)sh_malloc(sizeof(t_fork));
		new->pid = child;
		new->next = g_fork;
		g_fork = new;
		return (child);
	}
	return (0);
}

int		unset_fork(int pid)
{
	t_fork		*lst;
	t_fork		*prev;

	lst = g_fork;
	prev = NULL;
	while (lst)
	{
		if (lst->pid == pid)
		{
			if (lst->next && prev)
				prev->next = lst->next;
			if (g_fork == lst)
				g_fork = NULL;
			free(lst);
		}
		prev = lst;
		lst = lst->next;
	}
	return (0);
}
