/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_output.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 15:13:28 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/26 23:43:07 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <term.h>
#include <unistd.h>
#include <libft.h>
#include <termsh.h>

void		print_lst(t_cursor *cursor)
{
	int			n_line;
	t_char		*tmp;

	tmp = cursor->current;
	n_line = 0;
	tputs(tgetstr("sc", NULL), 1, ft_putchar);
	while (tmp)
	{
		write(1, &(tmp->c), 1);
		tmp = tmp->next;
		if (cursor->pos_col % (int)g_winsize.ws_col == 0)
			n_line++;
	}
	while (n_line--)
		tputs(tgetstr("up", NULL), 1, ft_putchar);
	tputs(tgetstr("rc", NULL), 1, ft_putchar);
}
