/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_func2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/11 15:00:09 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/25 16:23:25 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "analyser.h"

int			pars_cmd(t_tree *tree)
{
	if (!tree)
		return (1);
	tree->type = _word;
	return (1);
}

int			pars_left_mid(t_tree *tree, t_lex *tmp)
{
	if (tmp->next->type != _word)
		return (pars_error(PARS_LEFT_USAGE));
	if (!pars_tree_add(tree, tmp, _left))
		return (0);
	return (1);
}

int			pars_left_first(t_tree *tree, t_lex *tmp)
{
	tree->list = tmp;
	if (tmp->next->type != _word || tmp->next->next->type != _word)
		return (pars_error(PARS_LEFT_USAGE));
	if (!(tree->left = pars_tree_new(tmp->next)))
		return (0);
	if (!(tree->right = pars_tree_new(tmp->next->next)))
		return (0);
	tree->type = _left;
	tmp->next->next = NULL;
	tmp->next = NULL;
	return (1);
}

int			pars_left(t_tree *tree)
{
	t_lex	*tmp;

	if (!tree)
		return (1);
	tmp = tree->list;
	while (tmp && tmp->type != _left)
		tmp = tmp->next;
	if (!tmp)
		return (pars_cmd(tree));
	if (lex_lst_len(tree->list) < 3)
		return (pars_error(PARS_LEFT_USAGE));
	if (tmp == tree->list && !(pars_left_first(tree, tmp)))
		return (0);
	else if (tmp != tree->list && !pars_left_mid(tree, tmp))
		return (0);
	tree->left->type = _word;
	tree->right->type = _word;
	if (!pars_left(tree->right))
		return (0);
	return (1);
}
