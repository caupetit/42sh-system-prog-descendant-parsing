/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_pipe.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/25 20:26:11 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/26 21:54:33 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <analyser.h>
#include <shell.h>

void			exec_pipe_child(t_tree *tree, int pipefd[2])
{
	dup2(pipefd[1], 1);
	close(pipefd[0]);
	exec_tree(tree->left);
}

void			exec_pipe_father(t_tree *tree, int pipefd[2])
{
	dup2(pipefd[0], 0);
	close(pipefd[1]);
	exec_tree(tree->right);
}

void			exec_pipe(t_tree *tree)
{
	int			pipefd[2];
	pid_t		pid;
	pid_t		pid2;

	if ((pid = my_fork()) == 0)
	{
		if (pipe(pipefd) != 0)
			msg_error(102, NULL);
		if ((pid2 = my_fork()) == 0)
		{
			exec_pipe_child(tree, pipefd);
			exit(g_last_return);
		}
		else
		{
			my_wait(pid2);
			exec_pipe_father(tree, pipefd);
		}
		exit(g_last_return);
	}
	else
		my_wait(pid);
}
