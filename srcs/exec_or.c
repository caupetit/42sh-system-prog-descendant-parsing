/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_or.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 19:03:22 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/26 19:03:23 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <shell.h>

void		exec_or(t_tree *tree)
{
	exec_tree(tree->left);
	if (g_last_return)
		exec_tree(tree->right);
}
