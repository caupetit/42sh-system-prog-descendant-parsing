/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   analyser.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/11 14:52:17 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/26 23:02:37 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <analyser.h>

int			pars_error(char *str)
{
	ft_printf("Pars error: %s\n", str);
	return (0);
}

t_tree		*pars(t_lex *l_lex)
{
	t_tree	*tree;

	if (!(tree = pars_tree_new(l_lex)))
		return (NULL);
	if (!pars_and(tree))
	{
		pars_tree_del(&tree);
		return (NULL);
	}
	return (tree);
}

t_tree		*analyzer(char *line)
{
	t_tree	*tree;
	t_auto	aut;
	int		i;
	int		size;

	size = ft_strlen(line);
	if (!lex_init_auto(&aut, size))
		return (0);
	i = -1;
	while (++i <= size)
	{
		if (!lex_automaton(&aut, line[i]))
		{
			ft_strdel(&(aut.buf));
			return (0);
		}
	}
	ft_strdel(&(aut.buf));
	tree = pars(aut.l_lex);
	return (tree);
}
