/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_and.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/25 20:18:00 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/25 20:22:50 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <shell.h>

void		exec_and(t_tree *tree)
{
	exec_tree(tree->left);
	if (g_last_return == 0)
		exec_tree(tree->right);
}
