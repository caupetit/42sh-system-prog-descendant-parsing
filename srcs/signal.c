/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 10:01:17 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 21:54:17 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include <shell.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <libft.h>

#include <unistd.h>

static void			void42(int n)
{
	(void)n;
}

void				init_signal(void)
{
	signal(SIGINT, void42);
}
