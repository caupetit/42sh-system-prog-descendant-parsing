/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 14:27:07 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 16:46:46 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <term.h>
#include <unistd.h>
#include <stdlib.h>
#include <libft.h>
#include <sys/ioctl.h>
#include <termsh.h>
#include <shell.h>

t_char		*new_char(char c)
{
	t_char		*new;

	new = (t_char *)sh_malloc(sizeof(t_char));
	if (!new)
		msg_error(103, NULL);
	new->c = c;
	new->next = NULL;
	new->prev = NULL;
	return (new);
}

t_cursor	*init_cursor(int len)
{
	t_cursor		*new;

	new = (t_cursor *)sh_malloc(sizeof(t_cursor));
	if (!new)
		msg_error(103, NULL);
	new->start = new_char('\0');
	new->current = new->start;
	new->end = new->current;
	new->pos_col = len;
	return (new);
}

void		insert_char(t_cursor *cursor, char c)
{
	t_char		*new;

	new = new_char(c);
	new->next = cursor->current;
	new->prev = cursor->current->prev;
	if (cursor->current == cursor->start)
		cursor->start = new;
	cursor->current->prev = new;
	if (new->prev)
		new->prev->next = new;
	cursor->pos_col += 1;
	if (cursor->pos_col == 0)
		tputs(tgetstr("do", NULL), 1, ft_putchar);
	write(1, &c, 1);
	if (cursor->current != cursor->end)
		print_lst(cursor);
}
