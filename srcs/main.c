/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 16:10:28 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/26 23:02:43 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include <libft.h>
#include <shell.h>
#include <termsh.h>
#include <analyser.h>

t_env				*g_env = NULL;
t_fork				*g_fork = NULL;
unsigned int		g_last_return = 0;
struct termios		*g_backup_term;
struct winsize		g_winsize;

int			main(int argc, char **argv, char **envp)
{
	char		*line;
	t_tree		*cmd;

	g_env = init_env(envp);
	init_winsize(&g_winsize);
	if (!g_env)
		msg_error(1, NULL);
	(void)argc;
	(void)argv;
	init_signal();
	line = NULL;
	while (prompt(&line))
	{
		if (line && *line)
			cmd = analyzer(line);
		ft_strdel(&line);
		if (cmd)
			exec_tree(cmd);
		pars_tree_del(&cmd);
	}
	return (1);
}
