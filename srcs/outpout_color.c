/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   outpout_color.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/24 08:58:01 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 16:38:12 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>

int		putchar_col(int c, int color, int state, int fd)
{
	ft_putstr_fd("\033[", fd);
	ft_putnbr_fd(state, fd);
	ft_putchar_fd(';', fd);
	ft_putnbr_fd(color, fd);
	ft_putchar_fd('m', fd);
	return (write(fd, &c, 1) + ft_putstr_fd("\033[0m", fd));
}

int		putstr_col(char *str, int color, int state, int fd)
{
	if (!str)
		return (0);
	ft_putstr_fd("\033[", fd);
	ft_putnbr_fd(state, fd);
	ft_putchar_fd(';', fd);
	ft_putnbr_fd(color, fd);
	ft_putchar_fd('m', fd);
	return (write(fd, str, ft_strlen(str)) + ft_putstr_fd("\033[0m", fd) - 4);
}

int		putendl_col(char *str, int color, int state, int fd)
{
	if (!str)
		return (0);
	return (putstr_col(str, color, state, fd) + ft_putchar_fd('\n', fd));
}
