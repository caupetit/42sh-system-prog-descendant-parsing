/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_tree.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/11 14:42:43 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/27 20:49:29 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <ft_printf.h>
#include <libft.h>
#include <analyser.h>

t_tree		*pars_tree_new(t_lex *l_lex)
{
	t_tree	*elem;

	if (!(elem = (t_tree *)malloc(sizeof(t_tree))))
		return (NULL);
	elem->list = NULL;
	elem->right = NULL;
	elem->left = NULL;
	elem->type = 0;
	if (l_lex)
		elem->list = l_lex;
	return (elem);
}

int			pars_tree_add(t_tree *tree, t_lex *tmp, int type)
{
	t_tree		*elem;

	if (!(elem = pars_tree_new(tree->list)))
		return (0);
	tree->left = elem;
	elem = NULL;
	if (tmp->prev)
		(tmp->prev)->next = NULL;
	if (tmp->next)
	{
		if (!(elem = pars_tree_new(tmp->next)))
			return (0);
		tmp->next = NULL;
	}
	tree->right = elem;
	tree->type = type;
	tree->list = tmp;
	return (1);
}

void		pars_tree_del(t_tree **tree)
{
	if (!*tree)
		return ;
	lex_lst_del(&(*tree)->list);
	pars_tree_del(&(*tree)->left);
	pars_tree_del(&(*tree)->right);
	free(*tree);
	*tree = NULL;
}
