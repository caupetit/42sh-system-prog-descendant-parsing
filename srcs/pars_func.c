/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_func.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/11 14:55:54 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/25 16:23:24 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <analyser.h>

int			pars_and(t_tree *tree)
{
	t_lex	*tmp;

	if (!tree)
		return (1);
	tmp = tree->list;
	while (tmp && tmp->type != _and)
		tmp = tmp->next;
	if (!tmp)
		return (pars_sep(tree));
	if (!tmp->prev || tmp->prev->type != _word)
		return (pars_error(PARS_AND_NOCMD));
	if (!tmp->next || !(tmp->next->type == _word || tmp->next->type == _left))
		return (pars_error(PARS_AND_NOCMD));
	if (!pars_tree_add(tree, tmp, _and))
		return (0);
	if (!pars_sep(tree->left))
		return (0);
	if (!pars_and(tree->right))
		return (0);
	return (1);
}

int			pars_sep(t_tree *tree)
{
	t_lex	*tmp;

	if (!tree)
		return (1);
	tmp = tree->list;
	while (tmp && tmp->type != _sep)
		tmp = tmp->next;
	if (!tmp)
		return (pars_or(tree));
	if (!pars_tree_add(tree, tmp, _sep))
		return (0);
	if (tree->left && tree->left->list->type == _sep)
	{
		free(tree->left);
		tree->left = NULL;
	}
	if (!pars_or(tree->left))
		return (0);
	if (!pars_sep(tree->right))
		return (0);
	return (1);
}

int			pars_or(t_tree *tree)
{
	t_lex	*tmp;

	if (!tree)
		return (1);
	tmp = tree->list;
	while (tmp && tmp->type != _or)
		tmp = tmp->next;
	if (!tmp)
		return (pars_right(tree));
	if (!tmp->prev || tmp->prev->type != _word)
		return (pars_error(PARS_OR_NOCMD));
	if (!tmp->next || tmp->next->type != _word)
		return (pars_error(PARS_OR_NOCMD));
	if (!pars_tree_add(tree, tmp, _or))
		return (0);
	if (!pars_right(tree->left))
		return (0);
	if (!pars_or(tree->right))
		return (0);
	return (1);
}

int			pars_right(t_tree *tree)
{
	t_lex	*tmp;

	if (!tree)
		return (1);
	tmp = tree->list;
	while (tmp && tmp->type != _right)
		tmp = tmp->next;
	if (!tmp)
		return (pars_pipe(tree));
	if (!tmp->prev || tmp->prev->type != _word)
		return (pars_error(PARS_RIGHT_NOCMD));
	if (!tmp->next || tmp->next->type != _word)
		return (pars_error(PARS_RIGHT_NOFILE));
	if (!pars_tree_add(tree, tmp, _right))
		return (0);
	if (!pars_pipe(tree->left))
		return (0);
	if (!pars_right(tree->right))
		return (0);
	return (1);
}

int			pars_pipe(t_tree *tree)
{
	t_lex	*tmp;

	if (!tree)
		return (1);
	tmp = tree->list;
	while (tmp && tmp->type != _pipe)
		tmp = tmp->next;
	if (!tmp)
		return (pars_left(tree));
	if (!tmp->prev || tmp->prev->type != _word)
		return (pars_error(PARS_PIPE_NOCMD));
	if (!tmp->next || tmp->next->type != _word)
		return (pars_error(PARS_PIPE_NOCMD));
	if (!pars_tree_add(tree, tmp, _pipe))
		return (0);
	if (!pars_left(tree->left))
		return (0);
	if (!pars_pipe(tree->right))
		return (0);
	return (1);
}
