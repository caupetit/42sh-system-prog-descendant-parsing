/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 20:54:06 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 19:41:51 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <shell.h>

void		setenv_exec(char *name, char *value, int over)
{
	t_env		*env;
	t_env		*new;

	env = get_var_env(name);
	if (env && over == 1)
	{
		free(env->value);
		env->value = ft_strdup(value);
	}
	else if (!env)
	{
		env = g_env;
		while (env && env->next)
			env = env->next;
		new = (t_env *)malloc(sizeof(t_env));
		new->name = ft_strdup(name);
		new->value = ft_strdup(value);
		new->prev = env;
		new->next = NULL;
		if (env)
			env->next = new;
		else
			g_env = new;
	}
}

int			setenv42(char **args)
{
	int			i;

	i = 0;
	while (args && args[i])
		i++;
	if (i != 3 && i != 4)
		msg_error(3, NULL);
	else
		setenv_exec(args[1], args[2], ft_atoi(args[3]));
	return (1);
}
