/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_rright.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 14:30:04 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/27 19:19:40 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "analyser.h"
#include "shell.h"
#include "libft.h"

static int	error(int ret, char *err, char *file)
{
	ft_putstr_fd("Error: > or >>, ", 2);
	ft_putstr_fd(err, 2);
	ft_putendl_fd(file, 2);
	return (ret);
}

static int	open_file(char *file, char *type)
{
	int		fd;

	ft_putendl(type);
	if (type && type[1])
	{
		if ((fd = open(file, O_CREAT | O_APPEND | O_WRONLY, RIGHTS)) < 0)
			return (error(0, "couldn't open file: ", file));
	}
	else
	{
		if ((fd = open(file, O_CREAT | O_TRUNC | O_WRONLY, RIGHTS)) < 0)
			return (error(0, "couldn't open file: ", file));
	}
	return (fd);
}

static int	get_fd(t_tree *tree)
{
	int		fd;
	char	*file;
	t_tree	*tmp;
	char	*token;

	token = tree->list->str;
	tmp = tree;
	while (tmp && tmp->type == _right)
	{
		file = tmp->left->list->str;
		if (!(fd = open_file(file, tmp->list->str)))
			return (0);
		token = tmp->list->str;
		tmp = tmp->right;
		close(fd);
	}
	file = tmp->list->str;
	fd = open_file(file, token);
	return (fd);
}

void		exec_rright(t_tree *tree)
{
	int		fd;
	int		tmp_fd;

	if (!(fd = get_fd(tree)))
		return ;
	tmp_fd = dup(1);
	dup2(fd, 1);
	exec_tree(tree->left);
	dup2(tmp_fd, 1);
	close(fd);
}
