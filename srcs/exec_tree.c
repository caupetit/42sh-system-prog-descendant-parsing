/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_tree.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/06 09:49:16 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/26 22:23:11 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/wait.h>
#include <unistd.h>
#include <analyser.h>
#include <libft.h>
#include <shell.h>

void			exec_tree(t_tree *tree)
{
	if (!tree)
		ft_putstr("Tree NULL");
	else if (tree->type == _sep)
	{
		exec_tree(tree->left);
		exec_tree(tree->right);
	}
	else if (tree->type == _pipe)
		exec_pipe(tree);
	else if (tree->type == _and)
		exec_and(tree);
	else if (tree->type == _or)
		exec_or(tree);
	else if (tree->type == _left)
		exec_rleft(tree);
	else if (tree->type == _right)
		exec_rright(tree);
	else if (tree->type == _word)
		exec_word(tree);
}
