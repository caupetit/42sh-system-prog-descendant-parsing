/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/01 19:39:02 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 19:07:29 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>
#include <termsh.h>
#include <shell.h>

void			prompt_pwd(void)
{
	t_env		*pwd;
	t_env		*home;
	char		*tmp;

	pwd = get_var_env("PWD");
	home = get_var_env("HOME");
	tmp = NULL;
	if (home && pwd)
		tmp = ft_strstr(pwd->value, home->value);
	if (tmp)
	{
		putchar_col('~', BLACK, BOLD, 1);
		putendl_col(tmp + ft_strlen(home->value), BLACK, BOLD, 1);
	}
	else if (pwd)
		putendl_col(pwd->value, BLACK, BOLD, 1);
}

int				prompt_user(void)
{
	t_env		*usr;

	usr = get_var_env("USER");
	if (usr)
		return (putstr_col(usr->value, CYAN, NORMAL, 1));
	return (0);
}

int				prompt_host(void)
{
	char		*hostname;
	int			i;

	hostname = ft_strnew(1024);
	gethostname(hostname, 1024);
	i = 0;
	while (hostname[i] && hostname[i] != '.')
		putchar_col(hostname[i++], CYAN, NORMAL, 1);
	ft_strdel(&hostname);
	return (i);
}

int				prompt(char **line)
{
	int				len;
	static int		i = 0;

	prompt_pwd();
	if (i == 0)
		i = init_term();
	len = 0;
	len += prompt_user();
	if (len)
		len += putstr_col("@", CYAN, NORMAL, 1);
	len += prompt_host();
	len += putstr_col(" $", YELLOW, BOLD, 1);
	if (i == 0)
	{
		*line = get_line(len);
		exit_term();
	}
	else
		get_next_line(0, line);
	ft_putchar('\n');
	return (len);
}
