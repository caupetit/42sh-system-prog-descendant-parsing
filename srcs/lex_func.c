/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_func.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/05 16:54:08 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/26 20:22:18 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "analyser.h"

int			lex_wait(t_auto *aut, char c)
{
	if (ft_iswhite(c))
		return (1);
	if (!c)
		aut->status = _end;
	else if (c == '&')
		aut->status = _and;
	else if (c == ';')
		aut->status = _sep;
	else if (c == '|')
		aut->status = _pipe;
	else if (c == '>')
		aut->status = _right;
	else if (c == '<')
		aut->status = _left;
	else
		aut->status = _word;
	aut->buf[aut->i] = c;
	aut->i += 1;
	return (1);
}

int			lex_and(t_auto *aut, char c)
{
	if (aut->i == 1 && c != '&')
		return (lex_error(aut));
	aut->buf[aut->i] = c;
	aut->i += 1;
	if (!lex_add_token(aut))
		return (0);
	aut->status = _wait;
	return (1);
}

int			lex_sep(t_auto *aut, char c)
{
	if (!lex_add_token(aut))
		return (0);
	aut->status = _wait;
	return (lex_wait(aut, c));
}

int			lex_or(t_auto *aut, char c)
{
	if (!lex_add_token(aut))
		return (0);
	aut->status = _wait;
	return (lex_wait(aut, c));
}

int			lex_right(t_auto *aut, char c)
{
	if (aut->i == 1 && c == '>')
	{
		aut->buf[aut->i] = c;
		aut->i += 1;
		if (!lex_add_token(aut))
			return (0);
		aut->status = _wait;
		return (1);
	}
	if (!lex_add_token(aut))
		return (0);
	aut->status = _wait;
	return (lex_wait(aut, c));
}
