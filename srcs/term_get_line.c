/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_line.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 13:42:33 by rbernand          #+#    #+#             */
/*   Updated: 2014/03/27 17:41:51 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <unistd.h>
#include <termsh.h>
#include <shell.h>

void		exec_key(char *key, t_cursor *cursor)
{
	if (K1 == 0 && K2 == 0 && ft_isprint(K0) && K3 == 0)
		insert_char(cursor, K0);
	else if (K0 == 27 && K1 == 91 && (K2 <= 68 || K2 >= 67) && K3 == 0)
		move_cursor(cursor, key);
	else if (K0 == 127 && K1 == 0 && K2 == 0 && K3 == 0)
		term_backspace(cursor);
	else if (K0 == 27 && K1 == 91 && K2 == 51 && K3 == 126)
		term_delete(cursor);
	else if (K0 == 4 && K1 == 0 && K2 == 0 && K3 == 0)
		term_eof();
}

char		*convert(t_cursor *cursor)
{
	int			len;
	t_char		*tmp;
	char		*str;

	len = 1;
	tmp = cursor->start;
	while (tmp->next)
	{
		len++;
		tmp = tmp->next;
	}
	str = (char *)sh_malloc(sizeof(char) * len);
	tmp = cursor->start;
	len = 0;
	while (tmp)
	{
		str[len] = tmp->c;
		len++;
		tmp = tmp->next;
	}
	return (str);
}

void		char_lst_del(t_cursor *cursor)
{
	t_char		*tmp;
	t_char		*todel;

	tmp = cursor->start;
	while (tmp)
	{
		todel = tmp;
		tmp = tmp->next;
		free(todel);
	}
}

char		*get_line(int len_prompt)
{
	t_cursor		*cursor;
	char			*line;
	char			key[4];

	ft_bzero(key, 4);
	cursor = init_cursor(len_prompt);
	while (read(0, key, 4) != -1 && !(K0 == 10 && K1 == 0 && K2 == 0))
	{
		exec_key(key, cursor);
		ft_bzero(key, 4);
	}
	line = convert(cursor);
	char_lst_del(cursor);
	free(cursor);
	return (line);
}
